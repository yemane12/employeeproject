
import React, { Component } from 'react'
import 'bulma/css/bulma.css'
import { connect } from 'react-redux'
import { addEmployee, toggleEmployee, deleteEmployee, fetchEmployees } from './actions/Employees';

const Employee = ({ Employee, id, onDelete, onToggle }) => (
  <div className="box Employee-item level is-mobile">
    <div className="level-left">
      <label className={`level-item Employee-description ${Employee.done && 'completed'}`}>
        <input className="checkbox" type="checkbox" checked={Employee.done} onChange={onToggle}/>
        <span>{Employee.description}</span>
      </label>
    </div>
    <div className="level-right">
      <a className="delete level-item" onClick={onDelete}>Delete</a>
    </div>
  </div>
)

class Employees extends Component {
  state = { newEmployee: '' }

  componentDidMount() {
    this.props.fetchEmployees()
  }

  addEmployee (event) {
    event.preventDefault() // Prevent form from reloading page
    const { newEmployee } = this.state

    if(newEmployee) {
      const Employee = { description: newEmployee, done: false }
      this.props.addEmployee(Employee)
      this.setState({ newEmployee: '' })
    }
  }

  render() {
    let { newEmployee } = this.state
    const { Employees, isLoading, isSaving, error, deleteEmployee, toggleEmployee } = this.props

    const total = Employees.length
    const complete = Employees.filter((Employee) => Employee.done).length
    const incomplete = Employees.filter((Employee) => !Employee.done).length

    return (
      <section className="section full-column">
        <h1 className="title white">Employees</h1>
        <div className="error">{error}</div>

        <form className="form" onSubmit={this.addEmployee.bind(this)}>
          <div className="field has-addons" style={{ justifyContent: 'center' }}>
            <div className="control">
              <input className="input"
                     value={newEmployee}
                     placeholder="New Employee"
                     onChange={(e) => this.setState({ newEmployee: e.target.value })}/>
            </div>

            <div className="control">
              <button className={`button is-success ${(isLoading || isSaving) && "is-loading"}`}
                      disabled={isLoading || isSaving}>Add</button>
            </div>
          </div>
        </form>

        <div className="container Employee-list">
          {Employees.map((Employee) => <Employee key={Employee._id}
                                     id={Employee._id}
                                     Employee={Employee}
                                     onDelete={() => deleteEmployee(Employee._id)}
                                     onToggle={() => toggleEmployee(Employee._id)}/> )}
          <div className="white">
            Total: {total}  , Complete: {complete} , Incomplete: {incomplete}
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    Employees: state.Employees.items,
    isLoading: state.Employees.loading,
    isSaving: state.Employees.saving,
    error: state.Employees.error
  }
}

const mapDispatchToProps = {
  addEmployee,
  toggleEmployee,
  deleteEmployee,
  fetchEmployees
}

export default connect(mapStateToProps, mapDispatchToProps)(Employees)
