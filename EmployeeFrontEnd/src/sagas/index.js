import { call, put, takeLatest, takeEvery } from 'redux-saga/effects'
import { ADD_Employee, DELETE_Employee, TOGGLE_Employee, FETCH_EmployeeS, loadedEmployees, addEmployeeSuccess, EmployeesFailure } from '../actions/Employee'

function* getAllEmployee () {
  try {
    const res = yield call(fetch, 'v1/Employees')
    const Employees = yield res.json()
    yield put(loadedEmployees(Employees))
  } catch (e) {
    yield put(EmployeesFailure(e.message))
  }
}

function* saveEmployee (action) {
  try {
    const options = {
      method: 'POST',
      body: JSON.stringify(action.Employee),
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    }

    const res = yield call(fetch, 'v1/Employees', options)
    const Employee = yield res.json()
    yield put(addEmployeeSuccess(Employee))
  } catch (e) {
    yield put(EmployeeFailure(e.message))
  }
}

function* deleteEmployee (action) {
  try {
    yield call(fetch, `v1/Employee/${action.id}`, { method: 'DELETE' })
  } catch (e) {
    yield put(EmployeesFailure(e.message))
  }
}

function* updateEmployee (action) {
  try {
    yield call(fetch, `v1/Employee/${action.id}`, { method: 'POST' })
  } catch (e) {
    yield put(EmployeesFailure(e.message))
  }
}

function* rootSaga() {
  yield takeLatest(FETCH_EmployeeS, getAll)
  yield takeLatest(ADD_Employee, saveEmployee);
  yield takeLatest(DELETE_Employee, deleteEmployee);
  yield takeEvery(TOGGLE_Employee, updateEmployee);
}

export default rootSaga;
