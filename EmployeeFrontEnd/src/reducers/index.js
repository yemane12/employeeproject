import { combineReducers } from "redux";
import { postEmployee } from "./postEmployee";

export const reducers = combineReducers({
    postEmployee
})