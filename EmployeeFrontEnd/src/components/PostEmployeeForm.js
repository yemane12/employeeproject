import React, { useEffect, useState } from "react";
import { TextField, withStyles, Button } from "@material-ui/core";
import useForm from "./useForm";
import { connect } from "react-redux";
import * as actions from "../actions/postEmployee";
import ButterToast, { Cinnamon } from "butter-toast";
import { AssignmentTurnedIn } from "@material-ui/icons";

const initialFieldValues = {
    name: '',
    birthDate: '',
	gender: '',
	salary: ''
}

const styles = theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1)
        },
    },
    form: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    postBtn: {
        width: "50%"
    }
})

const PostEmployeeForm = ({ classes, ...props }) => {

    useEffect(() => {
        if (props.currentId != 0){
            setValues({
                ...props.postMessageList.find(x => x._id == props.currentId)
            })
            setErrors({})
        }
    }, [props.currentId])

    const validate = () => {
        let temp = { ...errors }
        temp.name = values.name ? "" : "This field is required."
        temp.birthDate = values.birthDate ? "" : "This field is required."
        temp.gender = values.gender ? "" : "This field is required."
        temp.salary = values.salary ? "" : "This field is required."
        setErrors({
            ...temp
        })
        return Object.values(temp).every(x => x == "")
    }

    var {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm
    } = useForm(initialFieldValues,props.setCurrentId)

    const handleSubmit = e => {
        e.preventDefault()
        const onSuccess = () => {
            ButterToast.raise({
                content: <Cinnamon.Crisp title="Employee Crud"
                    content="Submitted successfully"
                    scheme={Cinnamon.Crisp.SCHEME_PURPLE}
                    icon={<AssignmentTurnedIn />}
                />
            })
            resetForm()
        }
        if (validate()) {
            if (props.currentId == 0)
                props.createPostEmployee(values, onSuccess)
            else
                props.updatePostEmployee(props.currentId, values, onSuccess)
        }
    }

    return (
        <form autoComplete="off" noValidate className={`${classes.root} ${classes.form}`}
            onSubmit={handleSubmit}>
            <TextField
                name="name"
                variant="outlined"
                label="name"
                fullWidth
                value={values.name}
                onChange={handleInputChange}
                {...(errors.name && { error: true, helperText: errors.name })}
            />
            <TextField
                name="birthDate"
                variant="outlined"
                label="birthDate"
                fullWidth
                // multiline
                // rows={4}
                value={values.birthDate}
                onChange={handleInputChange}
                {...(errors.birthDate && { error: true, helperText: errors.birthDate })}
            />
            <TextField
                name="gender"
                variant="outlined"
                label="gender"
                fullWidth
                value={values.gender}
                onChange={handleInputChange}
                {...(errors.gender && { error: true, helperText: errors.gender })}
            />
            <TextField
                name="salary"
                variant="outlined"
                label="salary"
                fullWidth
                // multiline
                // rows={4}
                value={values.salary}
                onChange={handleInputChange}
                {...(errors.salary && { error: true, helperText: errors.salary })}
            />
            <Button
                variant="contained"
                color="primary"
                size="large"
                type="submit"
                className={classes.postBtn}
            >Submit</Button>
        </form>
    );
}
const mapStateToProps = state => ({
    postMessageList: state.postEmployee.list
})

const mapActionToProps = {
    createPostEmployee: actions.create,
    updatePostEmployee: actions.update
}


export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(PostEmployeeForm));