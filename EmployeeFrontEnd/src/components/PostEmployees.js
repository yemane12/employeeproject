import React, { useEffect, useState, Fragment } from "react";
import { connect } from "react-redux";
import * as actions from "../actions/postEmployee";
import { Grid, Paper, withStyles, List, Divider, Button, Table } from "@material-ui/core";
import PostEmployeeForm from "./PostEmployeeForm";
import ButterToast, { Cinnamon } from "butter-toast";
import { DeleteSweep } from "@material-ui/icons";

const styles = theme => ({
    paper: {
        margin: theme.spacing(3),
        padding: theme.spacing(2)
    },
    smMargin: {
        margin: theme.spacing(3)
    },
    actionDiv: {
        textAlign: "center"
    },

})

const PostEmployees = ({ classes, ...props }) => {
    //const {classes, ...props} = props
    const [currentId, setCurrentId] = useState(0)

    useEffect(() => {
        props.fetchAllPostEmployees()
    }, [])//DidMount

    const onDelete = id => {
        const onSuccess = () => {
            ButterToast.raise({
                content: <Cinnamon.Crisp title="Employee crud"
                    content="Deleted successfully"
                    scheme={Cinnamon.Crisp.SCHEME_PURPLE}
                    icon={<DeleteSweep />}
                />
            })
        }
        if (window.confirm('Are you sure to delete this record?'))
            props.deletePostEmployee(id,onSuccess)
    }
    return (
        <Grid container>
            <Grid item xs={5}>
                <Paper className={classes.paper}>
                    <PostEmployeeForm {...{ currentId, setCurrentId }} />
                </Paper>
            </Grid>
            <div className="table-wrapper"><h></h><p></p><p>View Employee</p>
                <table border="1"overflow="hidden">
                   <thead>
                    <tr>
                     <th >
                         Name
                     </th>
                     <th>
                     BirthDate
                    </th>
                    <th>
                     Gender
                     </th>
                     <th>
                       Salary
                      </th>  
                      <th>
                    Actions
                     </th>  
                     </tr>
                 </thead>
                    <tbody>
                        {
                            
                            props.postMessageList.map((record, index) => {
                                return (
                                            <tr key={index}>
                                        
                                                <td>
                                                    {record.name}
                                                </td>
                                                <td>
                                                    {record.birthDate}
                                                </td>
                                                <td>
                                                    {record.gender}
                                                </td>
                                                <td>
                                                    {record.salary}
                                                </td> 
                                                <td className={classes.actionDiv}>
                                                    <Button variant="contained" color="primary" size="small"
                                                        className={classes.smMargin}
                                                        onClick={() => setCurrentId(record._id)}>
                                                        Edit
                                                    </Button>
                                                    /
                                                    <Button variant="contained" color="secondary" size="small"
                                                        className={classes.smMargin}
                                                        onClick={() => onDelete(record._id)}>
                                                        Delete
                                                    </Button>
                                                </td>
                                                
                                            </tr>
                                )
                            })
                        }
                    </tbody>
                    </table >
                    </div>
              </Grid>
    );
}

const mapStateToProps = state => ({
    postMessageList: state.postEmployee.list
})

const mapActionToProps = {
    fetchAllPostEmployees: actions.fetchAll,
    deletePostEmployee: actions.Delete
}

export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(PostEmployees));
