// action types
export const ADD_Employee = 'ADD_Employee'
export const ADD_Employee_SUCCESS = 'ADD_Employee_SUCCESS'
export const EmployeeS_FAILURE = 'EmployeeS_FAILURE'
export const TOGGLE_Employee = 'TOGGLE_Employee'
export const DELETE_Employee = 'DELETE_Employee'
export const LOADED_EmployeeS = 'LOADED_EmployeeS'
export const FETCH_EmployeeS = 'FETCH_EmployeeS'

// action creators
export function addEmployee(Employee) {
  return { type: ADD_Employee, Employee }
}

export function addEmployeeSuccess(Employee) {
  return { type: ADD_Employee_SUCCESS, Employee }
}

export function EmployeesFailure(error) {
  return { type: EmployeeS_FAILURE, error }
}

export function toggleEmployee(id) {
  return { type: TOGGLE_Employee, id }
}

export function deleteEmployee(id) {
  return { type: DELETE_Employee, id }
}

export function loadedEmployees(Employees) {
  return { type: LOADED_EmployeeS, Employees }
}

export function fetchEmployees() {
  return { type: FETCH_EmployeeS }
}
