
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let employeeSchema = new Schema({
    name: {type:String},
    birthDate: {type:String},
	gender: {type:String},
	salary: {type:String},
}, {
    collection: 'employees'
  })

module.exports = mongoose.model('Employee', employeeSchema)